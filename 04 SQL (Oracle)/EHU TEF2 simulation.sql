-- TEaSoF Simulation
-- Only includes UG non-franchise students
-- 'Course level'  is defined as HESA CAH groupings (using JACS)
-- Only FT included here (as prob with non completion rates)

-- Corse metrics 
  -- NSS
  -- HESA non-completion
  -- DLHE
-- Supplementary metrics
  -- LEO
  -- Grade inflation (we calculate this ourselves)
  -- Teaching Intensity (we calculate ourselves)
  
-- 2 potential models:
--two models:
  -- Model A: a �by exception� model giving a provider-level rating and giving subjects the same rating as the provider where metrics performance is similar, with fuller assessment (and potentially different ratings) where metrics performance differs.
  -- Model B: A �bottom-up� model fully assessing each subject to give subject-level ratings, feeding into the provider-level assessment and rating. Subjects are grouped for submissions, but ratings are still awarded at subject-level.


-- have to unpivot as structure of code not suitable for layout asked for by directorate (otherwise, have to rewrite everything -- MF 04/01/18

SELECT  TEF.*,
        BNCH_ALL.OUTPUT   BNCH_ALL,
        BNCH_NW.OUTPUT    BNCH_NW,
        STDDEV_ALL.OUTPUT    STDDEV_ALL
FROM
    -- EDGE HILL TEF FIGURES
    (select  distinct crse.FAC,	crse.PU,	crse.COURSE_DESC,	COURSE_COURSE_CODE, cah.CAH2, piv.*
    from    (select  cah, metric, output
            from VW_INTTEF_2017_CAH2
            UNPIVOT  (  output for metric in ("TEACHING_ON_COURSE","ASS_AND_FEEDBACK","ACA_SUPPORT","RET_PCT","WRK_STUD_PCT","PROFF_PCT","TEF_CORE_PCT")
                            )) piv,
            (select distinct cah, CAH2 from (select cah.*, SUBSTR(CAH.CAH2,2,8) cah from LKP_JACS_CAH   cah)) cah,
            (select distinct FAC,	PU,	COURSE_DESC,	COURSE_COURSE_CODE,	CAH  from VW_INTTEF_2017_CAH2) crse
    where   piv.cah =  CAH.CAH  (+) 
    and     cah.cah =  crse.cah (+))  TEF,
    -- BENCHMARKS FOR ALL HEIs
    (select  distinct crse.FAC,	crse.PU,	crse.COURSE_DESC,	COURSE_COURSE_CODE, cah.CAH2, piv.cah,
                (case when piv.metric like 'TOC_ALL_BNCH' then 'TEACHING_ON_COURSE'
                      when piv.metric like  'AF_ALL_BNCH' then 'ASS_AND_FEEDBACK'
                      when piv.metric like  'AS_ALL_BNCH' then 'ACA_SUPPORT'
                      when piv.metric like  'WRK_STUD_ALL_BNCH' then 'WRK_STUD_PCT'
                      when piv.metric like  'HIGH_ALL_BNCH' then 'PROFF_PCT'
                  else 'xx'
                  end) metric,
                  piv.output
    from    (select  cah, metric, output
            from VW_INTTEF_2017_CAH2
            UNPIVOT  (  output for metric in ("TOC_ALL_BNCH", "AF_ALL_BNCH", "AS_ALL_BNCH", "WRK_STUD_ALL_BNCH",  "HIGH_ALL_BNCH")
                            )) piv,
            (select distinct cah, cah2 from (select cah.*, SUBSTR(CAH.CAH2,2,8) cah from LKP_JACS_CAH   cah)) cah,
            (select distinct FAC,	PU,	COURSE_DESC,	COURSE_COURSE_CODE,	CAH  from VW_INTTEF_2017_CAH2) crse
    where   piv.cah =  CAH.CAH  (+) 
    and     cah.cah =  crse.cah (+)) BNCH_ALL,
    --BENCHMARKS FOR NW HEIs
    (select  distinct crse.FAC,	crse.PU,	crse.COURSE_DESC,	COURSE_COURSE_CODE, cah.CAH2, piv.cah,
                (case when piv.metric like  'TOC_NW_BNCH' then 'TEACHING_ON_COURSE'
                      when piv.metric like  'AF_NW_BNCH' then 'ASS_AND_FEEDBACK'
                      when piv.metric like  'AS_NW_BNCH' then 'ACA_SUPPORT'
                      when piv.metric like  'WRK_STUD_NW_BNCH' then 'WRK_STUD_PCT'
                      when piv.metric like  'HIGH_NW_BNCH' then 'PROFF_PCT'
                  else 'xx'
                  end) metric,
                  piv.output
    from    (select  cah, metric, output
            from VW_INTTEF_2017_CAH2
            UNPIVOT  (  output for metric in ("TOC_NW_BNCH", "AF_NW_BNCH", "AS_NW_BNCH", "WRK_STUD_NW_BNCH",  "HIGH_NW_BNCH")
                            )) piv,
            (select distinct cah, CAH2 from (select cah.*, SUBSTR(CAH.CAH2,2,8) cah from LKP_JACS_CAH   cah)) cah,
            (select distinct FAC,	PU,	COURSE_DESC,	COURSE_COURSE_CODE,	CAH  from VW_INTTEF_2017_CAH2) crse
    where   piv.cah =  CAH.CAH  (+) 
    and     cah.cah =  crse.cah (+)) BNCH_NW,    
    --STANDARD DEVIATION FOR ALL HEIs
    (select  distinct crse.FAC,	crse.PU,	crse.COURSE_DESC,	COURSE_COURSE_CODE, cah.CAH2, piv.cah,
                (case when piv.metric like  'TOC_ALL_STDEV' then 'TEACHING_ON_COURSE'
                      when piv.metric like  'AF_ALL_STDEV' then 'ASS_AND_FEEDBACK'
                      when piv.metric like  'AS_ALL_STDEV' then 'ACA_SUPPORT'
                      when piv.metric like  'WRK_ALL_STDEV' then 'WRK_STUD_PCT'
                      when piv.metric like  'PROFF_ALL_STDEV' then 'PROFF_PCT'
                  else 'xx'
                  end) metric,
                  piv.output
    from    (select  cah, metric, output
            from VW_INTTEF_2017_CAH2
            UNPIVOT  (  output for metric in ("TOC_ALL_STDEV", "AF_ALL_STDEV", "AS_ALL_STDEV", "WRK_ALL_STDEV", "PROFF_ALL_STDEV")
                            )) piv,
            (select distinct cah, CAH2 from (select cah.*, SUBSTR(CAH.CAH2,2,8) cah from LKP_JACS_CAH   cah)) cah,
            (select distinct FAC,	PU,	COURSE_DESC,	COURSE_COURSE_CODE,	CAH  from VW_INTTEF_2017_CAH2) crse
    where   piv.cah =  CAH.CAH  (+) 
    and     cah.cah =  crse.cah (+)) STDDEV_ALL
--JOIN TO ALL HEIs BNCHMARKS
where   TEF.FAC	              =      BNCH_ALL.FAC         (+)
AND     TEF.PU	              =      BNCH_ALL.PU          (+)
AND     TEF.COURSE_DESC	      =      BNCH_ALL.COURSE_DESC    (+)
AND     TEF.COURSE_COURSE_CODE	   =      BNCH_ALL.COURSE_COURSE_CODE         (+)
AND     TEF.CAH2	            =      BNCH_ALL.CAH2        (+)
AND     TEF.cah	              =      BNCH_ALL.cah         (+)
AND     TEF.metric	          =      BNCH_ALL.metric      (+)
--JOIN TO NW HEIs BNCHMARKS
AND     TEF.FAC	              =      BNCH_NW.FAC         (+)
AND     TEF.PU	              =      BNCH_NW.PU          (+)
AND     TEF.COURSE_DESC	      =      BNCH_NW.COURSE_DESC    (+)
AND     TEF.COURSE_COURSE_CODE	   =      BNCH_NW.COURSE_COURSE_CODE         (+)
AND     TEF.CAH2	            =      BNCH_NW.CAH2        (+)
AND     TEF.cah	              =      BNCH_NW.cah         (+)
AND     TEF.metric	          =      BNCH_NW.metric      (+)
--JOIN TO ALL HEIs STANDARD DEVIATIONS
AND     TEF.FAC	              =      STDDEV_ALL.FAC         (+)
AND     TEF.PU	              =      STDDEV_ALL.PU          (+)
AND     TEF.COURSE_DESC	      =      STDDEV_ALL.COURSE_DESC    (+)
AND     TEF.COURSE_COURSE_CODE	   =      STDDEV_ALL.COURSE_COURSE_CODE         (+)
AND     TEF.CAH2	            =      STDDEV_ALL.CAH2        (+)
AND     TEF.cah	              =      STDDEV_ALL.cah         (+)
AND     TEF.metric	          =      STDDEV_ALL.metric      (+)
-- ONLY USE Z SCORE FOR ALL AS NOT ENOUGH nw INSTITUTIONS FOR EFFECTIVE ANALYSIS 
;


select * from VW_INTTEF_2017_CAH2;


--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_INTTEF_2017_CAH2" AS
select  distinct
        --courses.FAC,  courses.pu, courses.course_desc, courses.course_course_code, courses.jacs1,  -- for course level info        
        --remove courses info for summary output table data
        tef.cah, 
        cah.CAH2,
        TEACHING_ON_COURSE, ass_and_feedback, ACA_SUPPORT,
        ret_pct,
        WRK_STUD_PCT,  PROFF_PCT,
        TEF_CORE_PCT,
        INF,
        SUS_EMP,   ABOVE_MED_PCT,
        HC_1, TEACH_1, HC_2, TEACH_2, HC_3, TEACH_3,
        TOC_ALL_BNCH, AF_ALL_BNCH, AS_ALL_BNCH, WRK_STUD_ALL_BNCH,  HIGH_ALL_BNCH,          --    BENCHMARK 1:  ALL HEIs
          TOC_ALL_STDEV, AF_ALL_STDEV,  AS_ALL_STDEV, WRK_ALL_STDEV,  PROFF_ALL_STDEV,      -- STANDARD DEVIATION FOR Z SCORE
        TOC_NW_BNCH, AF_NW_BNCH, AS_NW_BNCH,  WRK_STUD_NW_BNCH,   HIGH_NW_BNCH             --    BENCHMARK 2:  NW HEIs
from
      (select  distinct  NSS.CAH,
              NSS.TEACHING_ON_COURSE, NSS.ass_and_feedback, NSS.ACA_SUPPORT,  --NSS CORE METRICS (1-3)
              cont.ret_pct,                                                   --NON CONT CORE METRIC (4)
              WRK_STUD_PCT,  PROFF_PCT,                                          --DLHE CORE METRIC (5-6)
              ROUND(((NSS.TEACHING_ON_COURSE + NSS.ass_and_feedback + NSS.ACA_SUPPORT + (cont.ret_pct + cont.ret_pct) + (WRK_STUD_PCT +  PROFF_PCT + WRK_STUD_PCT +  PROFF_PCT)) / 9)) TEF_CORE_PCT,
              inf.INF,                                                        --GRADE INFLATION SUPP METRIC (1)        
              LEO.SUS_EMP,   LEO.PCT ABOVE_MED_PCT,                           --LEO  SUPP METICS (2-3)
              HC_1, TEACH_1, HC_2, TEACH_2, HC_3, TEACH_3,                     --TEACHING INTENSITY  SUPP METICS (4-5)            
              TOC_ALL_BNCH, AF_ALL_BNCH, AS_ALL_BNCH, WRK_STUD_ALL_BNCH,  HIGH_ALL_BNCH,        --    BENCHMARK 1:  ALL HEIs
                TOC_ALL_STDEV, AF_ALL_STDEV,  AS_ALL_STDEV, WRK_ALL_STDEV,  PROFF_ALL_STDEV,      -- STANDARD DEVIATION FOR Z SCORE
              TOC_NW_BNCH, AF_NW_BNCH, AS_NW_BNCH,  WRK_STUD_NW_BNCH,   HIGH_NW_BNCH            --    BENCHMARK 2:  NW HEIs
      from    VW_TEF_NSS_2017_CAH2   nss,
              VW_TEF_DLHE_2017_CAH2  dlhe,
              VW_TEF_RET_2017_CAH2   cont,
              VW_TEF_INF_2017_CAH2   inf,
              VW_TEF_LEO_2017_CAH2   LEO,
              VW_TEF_TEACH_2017_CAH2   TEACH
      where   nss.CAH     =   dlhe.cAH    (+)   
      and     nss.CAH     =   cont.CAH    (+) 
      and     nss.CAH     =   inf.CAH     (+)
      AND     NSS.CAH     =   LEO.cah     (+)
      and     NSS.CAH     =   teach.cah   (+))  tef,
      (Select  distinct (case when faculty_desc like 'Faculty of Arts % Sciences' then 'FAS'
                              when faculty_desc like 'Faculty of Education' then 'FoE'
                              when faculty_desc like 'Faculty of Health % Social Care' then 'FoHSC'
                              else 'N/A'
                              end) FAC, 
                        pu, course_desc, course_course_code, jacs1,
                        (CASE when jacs1 like 'B7' then 'CAH02-01-01'
                              when jacs1 like 'X2' then 'CAH22-01-01'
                              when jacs1 like 'F8' then 'CAH12-01-02'
                              WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN COURSE_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END) CAH
                from    (select  distinct faculty_desc, pu, course_course_code, course_desc, 
                                          (case when course_level(hesa_qualification_aim) in ('PGR','PGT') then 'PG'
                                          else 'UG'
                                          end) UGPG 
                            from    mv_student_on_course_2017 soc,
                                    quercus_sppu.hesa_course hc
                            where     soc.course_obid = hc.course (+)
                            and       soc.ci_academic_year = hc.academic_session (+)
                            and       course_course_code not like 'BSD%'
                            and       scd_status_desc like 'Registered'
                            and       ci_instance_code not like '%TEMP'
                            and       ci_instance_code not like '%EXT'           ) soc,
                        (select   ep.programme_code,
                                      jacs1.jacs_subject jacs1,
                                      jacs2.jacs_subject jacs2,
                                      jacs3.jacs_subject jacs3
                            from      eval.programme@sppu_eval ep,
                                     (select
                                      max(pk),
                                      programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      from      eval.v_text_programme_versions@sppu_eval
                                      where archived is null
                                      group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      ) epv,
                                      quercus_sppu.jacs_subject jacs1,
                                      quercus_sppu.jacs_subject jacs2,
                                      quercus_sppu.jacs_subject jacs3
                            where   ep.pk                 =   epv.programme_pk  (+)
                            and     epv.primary_subject   =   jacs1.object_id   (+)
                            and     epv.secondary_subject =   jacs2.object_id   (+)
                            and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah1,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah2
    --2 CAH TABLES AS SOME COURSES AT DIFF JACS LEVEL
                      WHERE     SOC.course_course_code  =   JACS.programme_code         (+)
                      and       JACS.JACS1              =   CAH1.JCODE                  (+)
                      and       JACS.JACS1              =   CAH2.JCODE                  (+)
                      and       jacs.jacs1 is not null
                      and       UGPG = 'UG') courses,
       (select cah.*, 
               SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
               SUBSTR(CAH.CAH2,2,8)             CAH
        from   LKP_JACS_CAH  cah) cah
where courses.cah       =     tef.cah     (+)
and   courses.cah       =     cah.cah     (+)
and   TEF_CORE_PCT is not null

--ORDER BY fac, pu, course_desc
order by tef.cah
;
 





-- NSS
  --core metric 1: Teaching on my course (NSS Q1-4)
  --core metric 2: Assessment and feedback (2015 NSS and 2016 NSS Q5-9, subsequent NSS Q8-11)
  --core metric 3: Academic support (2015 NSS and 2016 NSS Q10-12, subsequent NSS Q12-14)

--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_NSS_2017_CAH2" AS
select nss.*,
        TOC_ALL_BNCH, AF_ALL_BNCH, AS_ALL_BNCH,
          TOC_ALL_STDEV, AF_ALL_STDEV,  AS_ALL_STDEV,
        TOC_NW_BNCH, AF_NW_BNCH, AS_NW_BNCH
from
  (SELECT  DISTINCT   CAH,
                    ROUND(AVG(TEACHING_ON_COURSE)) TEACHING_ON_COURSE,
                    ROUND(AVG(ass_and_feedback))   ass_and_feedback,
                    ROUND(AVG(ACA_SUPPORT))        ACA_SUPPORT           
      FROM
                  --get nss data.  from Jo's MyData records manually uploaded into tables
                  ( 
                                                             
              SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT              
                              -- cannot know which high lvl jac is linked to which low level CAH
                              -- have to conclude that all is applicaple 
                              -- this is not correct but it is consistant - will have to be acceptable
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from TBL_NSS_2016_ALL nss
                  where MOS like 'Full-time' and INSTITUTION like '%Edge Hill%') nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+)
              
              UNION
              
                 SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT              
                              -- cannot know which high lvl jac is linked to which low level CAH
                              -- have to conclude that all is applicaple 
                              -- this is not correct but it is consistant - will have to be acceptable
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from TBL_NSS_2017_ALL nss
                  where MOS like 'Full-time' and INSTITUTION like '%Edge Hill%') nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+) ) NSS
        GROUP BY   CAH   ) NSS,
  (SELECT  DISTINCT   CAH,
                    ROUND(AVG(TEACHING_ON_COURSE)) TOC_ALL_BNCH,          
                    ROUND(AVG(ass_and_feedback))   AF_ALL_BNCH,
                    ROUND(AVG(ACA_SUPPORT))        AS_ALL_BNCH,              
                    ROUND(STDDEV(TEACHING_ON_COURSE),2)    TOC_ALL_STDEV, 
                    ROUND(STDDEV(ass_and_feedback),2)    AF_ALL_STDEV,
                    ROUND(STDDEV(ACA_SUPPORT),2)    AS_ALL_STDEV  
                    -- include standard deviation, with mean, for z score calc
      FROM
                  --get nss data.  from Jo's MyData records manually uploaded into tables
                  ( 
                                                             
              SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT              
                              -- cannot know which high lvl jac is linked to which low level CAH
                              -- have to conclude that all is applicaple 
                              -- this is not correct but it is consistant - will have to be acceptable
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from TBL_NSS_2016_ALL nss
                  where MOS like 'Full-time') nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+)
              
              
              UNION
              
                 SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from TBL_NSS_2017_ALL nss
                  where MOS like 'Full-time') nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+)) NSS
        GROUP BY   CAH   ) all_bnch,
  (SELECT  DISTINCT   CAH,
                    ROUND(AVG(TEACHING_ON_COURSE)) TOC_NW_BNCH,            
                    ROUND(AVG(ass_and_feedback))   AF_NW_BNCH,
                    ROUND(AVG(ACA_SUPPORT))        AS_NW_BNCH          
      FROM
                  --get nss data.  from Jo's MyData records manually uploaded into tables
                  ( 
                                                             
              SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT              
                              -- cannot know which high lvl jac is linked to which low level CAH
                              -- have to conclude that all is applicaple 
                              -- this is not correct but it is consistant - will have to be acceptable
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from (select  distinct nss.*
                        from    TBL_NSS_2016_ALL nss,
                                (select distinct (case when   provider_name like 'The Manchester Metropolitan University' then 'Manchester Metropolitan University'
                                                       else   provider_name end) provider_name from  tbl_dlhe_detail_ft   dlhe
                                where north_west_heo like 'North West HEI'
                                and   MOS like 'Full-time') DLHE
                        where  nss.institution like dlhe.provider_name||'%') nss) nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+)
              
              
              UNION
              
                SELECT distinct cah.cah, institution, TEACHING_ON_CRSE TEACHING_ON_COURSE, ASSESSMENT_FEEDBACK ass_and_feedback, ACADEMIC_SUPPORT ACA_SUPPORT              
                              -- cannot know which high lvl jac is linked to which low level CAH
                              -- have to conclude that all is applicaple 
                              -- this is not correct but it is consistant - will have to be acceptable
              from
                  (select nss.*,  
                         -- manullay recode NSS headings
                         (case  when nss.subject LIKE 'Physical Geography and Environmental Science'	then	'PHYSICAL GEOGRAPHICAL SCIENCES'
                                      when nss.subject like	'History'	then	'HISTORY BY AREA'
                                      when nss.subject like	'Law'	then	'LAW BY AREA'
                                      when nss.subject like	'Sports Science'	then	'SPORT AND EXERCISE SCIENCE'
                                      when nss.subject like	'Initial Teacher Training'	then	'TRAINING TEACHERS'
                                      else upper(nss.subject)  end) jcode 
                  from (select  distinct nss.*
                        from    TBL_NSS_2017_ALL nss,
                                (select distinct (case when   provider_name like 'The Manchester Metropolitan University' then 'Manchester Metropolitan University'
                                                       else   provider_name end) provider_name from  tbl_dlhe_detail_ft   dlhe
                                where north_west_heo like 'North West HEI'
                                and   MOS like 'Full-time') DLHE
                        where  nss.institution like dlhe.provider_name||'%') nss) nss,
                  (select cah.*, upper(SUBSTR(replace(CAH.JACS_PRINCIPAL_SUBJECT, '&','and'),6)) JCODE, SUBSTR(CAH.CAH2,2,8) CAH from LKP_JACS_CAH cah) cah
              where   nss.jcode     =     cah.jcode    (+) ) NSS
        GROUP BY   CAH   ) NW_bnch
WHERe   nss.CAH     =     ALL_bnch.cah      (+)
AND     nss.CAH     =     NW_bnch.cah      (+)  
;


select * from VW_TEF_DLHE_2017
order by ;


-- DLHE
  --core metric 5: Employment or further study
  --core metric 6: Highly skilled employment or further study
-- FULL TIME ONLY
--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_DLHE_2017_CAH2" AS
SELECT  DLHE.CAH, WRK_STUD_PCT, PROFF_PCT ,
        WRK_STUD_ALL_BNCH,  HIGH_ALL_BNCH,
        -- include standard deviation for z score calc
          WRK_ALL_STDEV,  PROFF_ALL_STDEV,
        WRK_STUD_NW_BNCH,   HIGH_NW_BNCH
FROM
      (select  WRK.CAH,
                  WRK_STUD_PCT , STUD_PCT, PROFF_PCT 
          from
              (SELECT  CAH,
                      ROUND(AVG(WRK_STUD_PCT)) WRK_STUD_PCT
              FROM        
                (SELECT  CAH, PROVIDER_NAME,
                        ROUND((SUM(WORK_STUD) / SUM(TOTAL)*100)) WRK_STUD_PCT
                FROM        
                    (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, work_stud, TOTAL
                    FROM
                        (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(WORK_OR_STUDY) work_stud,  SUM(GRAND_TOTAL) TOTAL
                                      FROM          
                                          (select  dlhe.*,
                                                  ((CASE WHEN DUE_TO_START IS NOT NULL THEN DUE_TO_START ELSE 0 END) + 
                                                   (CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN WORKING_FULL_TIME IS NOT NULL THEN WORKING_FULL_TIME ELSE 0 END) + 
                                                   (CASE WHEN WORKING_PART_TIME IS NOT NULL THEN WORKING_PART_TIME ELSE 0 END)) WORK_OR_STUDY
                                          from    tbl_dlhe_detail_ft   dlhe
                                          where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                                and     MOS   like 'Full-time'
                                                AND     provider_name  like 'Edge Hill%')
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                        (select cah.*, 
                                          SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah
                    where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      WRK_STUDY 
                GROUP BY CAH, PROVIDER_NAME)
              GROUP BY CAH) WRK,
            (SELECT  CAH,
                    ROUND(AVG(STUD_PCT)) STUD_PCT
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(STUDY) / SUM(TOTAL)*100)) STUD_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, STUDY, TOTAL
                  FROM
                      (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(STUDY) STUDY,  SUM(GRAND_TOTAL) TOTAL
                                    FROM          
                                        (select  dlhe.*,
                                                ((CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                             (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END)) STUDY
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                              and     MOS   like 'Full-time'
                                              AND     provider_name  like 'Edge Hill%')
                      GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      STUDY 
              GROUP BY CAH, PROVIDER_NAME)
            GROUP BY CAH) STUDY,
            (SELECT  CAH,
                    ROUND(AVG(PROFF_PCT)) PROFF_PCT
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(PROFF) / SUM(TOTAL)*100)) PROFF_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, PROFF, TOTAL
                  FROM
                     (SELECT    PROFF.*,  TOTAL.TOTAL    FROM
                      (SELECT   JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) PROFF
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                          and     MOS   like 'Full-time'                                    
                                          AND     PROFESSIONAL_MARKER  =  'Professional employment'
                                          AND     provider_name  like 'Edge Hill%'
                                GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) PROFF,
                        (select  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) TOTAL
                        from    tbl_dlhe_detail_ft   dlhe  where   academic_year in (/*'2013/14', */'2014/15', '2015/16') and     MOS   like 'Full-time' AND provider_name  like 'Edge Hill%'   --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) TOTAL
                        WHERE   PROFF.PROVIDER_NAME                 =     TOTAL.PROVIDER_NAME     (+)
                        AND     PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     TOTAL.JACS_PRINCIPAL_SUBJECT_V3   (+) ) PROFF,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+) )      PROFF 
              GROUP BY CAH, PROVIDER_NAME) PROFF
            GROUP BY CAH) PROFF
          WHERE WRK.CAH   =   STUDY.CAH   (+)  
          AND   WRK.CAH   =   PROFF.CAH   (+))  DLHE,
      (select  WRK.CAH,
                  WRK_STUD_PCT  WRK_STUD_ALL_BNCH, STUD_PCT STUD_ALL_BNCH, PROFF_PCT HIGH_ALL_BNCH,
                  WRK_ALL_STDEV, STUD_ALL_STDEV,  PROFF_ALL_STDEV
          from
              (SELECT  CAH,
                      ROUND(AVG(WRK_STUD_PCT)) WRK_STUD_PCT,
                      ROUND(STDDEV(WRK_STUD_PCT),2)  WRK_ALL_STDEV
              FROM        
                (SELECT  CAH, PROVIDER_NAME,
                        ROUND((SUM(WORK_STUD) / SUM(TOTAL)*100)) WRK_STUD_PCT
                FROM        
                    (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, work_stud, TOTAL
                    FROM
                        (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(WORK_OR_STUDY) work_stud,  SUM(GRAND_TOTAL) TOTAL
                                      FROM          
                                          (select  dlhe.*,
                                                  ((CASE WHEN DUE_TO_START IS NOT NULL THEN DUE_TO_START ELSE 0 END) + 
                                                   (CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN WORKING_FULL_TIME IS NOT NULL THEN WORKING_FULL_TIME ELSE 0 END) + 
                                                   (CASE WHEN WORKING_PART_TIME IS NOT NULL THEN WORKING_PART_TIME ELSE 0 END)) WORK_OR_STUDY
                                          from    tbl_dlhe_detail_ft   dlhe
                                          where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                                and     MOS   like 'Full-time')
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                        (select cah.*, 
                                          SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah
                    where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      WRK_STUDY 
                GROUP BY CAH, PROVIDER_NAME)
              GROUP BY CAH) WRK,
            (SELECT  CAH,
                    ROUND(AVG(STUD_PCT)) STUD_PCT,
                    ROUND(STDDEV(STUD_PCT),2)  STUD_ALL_STDEV
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(STUDY) / SUM(TOTAL)*100)) STUD_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, STUDY, TOTAL
                  FROM
                      (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(STUDY) STUDY,  SUM(GRAND_TOTAL) TOTAL
                                    FROM          
                                        (select  dlhe.*,
                                                ((CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                             (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END)) STUDY
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                              and     MOS   like 'Full-time')
                      GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      STUDY 
              GROUP BY CAH, PROVIDER_NAME)
            GROUP BY CAH) STUDY,
            (SELECT  CAH,
                    ROUND(AVG(PROFF_PCT)) PROFF_PCT,
                    ROUND(STDDEV(PROFF_PCT),2)  PROFF_ALL_STDEV
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(PROFF) / SUM(TOTAL)*100)) PROFF_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, PROFF, TOTAL
                  FROM
                     (SELECT    PROFF.*,  TOTAL.TOTAL    FROM
                      (SELECT   JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) PROFF
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                          and     MOS   like 'Full-time'                                    
                                          AND     PROFESSIONAL_MARKER  =  'Professional employment'
                                GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) PROFF,
                        (select  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) TOTAL
                        from    tbl_dlhe_detail_ft   dlhe  where   academic_year in (/*'2013/14', */'2014/15', '2015/16') and     MOS   like 'Full-time'   --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) TOTAL
                        WHERE   PROFF.PROVIDER_NAME                 =     TOTAL.PROVIDER_NAME     (+)
                        AND     PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     TOTAL.JACS_PRINCIPAL_SUBJECT_V3   (+) ) PROFF,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+) )      PROFF 
              GROUP BY CAH, PROVIDER_NAME) PROFF
            GROUP BY CAH) PROFF
          WHERE WRK.CAH   =   STUDY.CAH   (+)  
          AND   WRK.CAH   =   PROFF.CAH   (+))  ALL_bnch,
      (select  WRK.CAH,
                  WRK_STUD_PCT  WRK_STUD_NW_BNCH, STUD_PCT STUD_NW_BNCH, PROFF_PCT HIGH_NW_BNCH
          from
              (SELECT  CAH,
                      ROUND(AVG(WRK_STUD_PCT)) WRK_STUD_PCT
              FROM        
                (SELECT  CAH, PROVIDER_NAME,
                        ROUND((SUM(WORK_STUD) / SUM(TOTAL)*100)) WRK_STUD_PCT
                FROM        
                    (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, work_stud, TOTAL
                    FROM
                        (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(WORK_OR_STUDY) work_stud,  SUM(GRAND_TOTAL) TOTAL
                                      FROM          
                                          (select  dlhe.*,
                                                  ((CASE WHEN DUE_TO_START IS NOT NULL THEN DUE_TO_START ELSE 0 END) + 
                                                   (CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END) + 
                                                   (CASE WHEN WORKING_FULL_TIME IS NOT NULL THEN WORKING_FULL_TIME ELSE 0 END) + 
                                                   (CASE WHEN WORKING_PART_TIME IS NOT NULL THEN WORKING_PART_TIME ELSE 0 END)) WORK_OR_STUDY
                                          from    tbl_dlhe_detail_ft   dlhe
                                          where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                                and     MOS   like 'Full-time'
                                                AND     NORTH_WEST_HEO  like 'North West HEI')
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                        (select cah.*, 
                                          SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah
                    where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      WRK_STUDY 
                GROUP BY CAH, PROVIDER_NAME)
              GROUP BY CAH) WRK,
            (SELECT  CAH,
                    ROUND(AVG(STUD_PCT)) STUD_PCT
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(STUDY) / SUM(TOTAL)*100)) STUD_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, STUDY, TOTAL
                  FROM
                      (SELECT   DISTINCT  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(STUDY) STUDY,  SUM(GRAND_TOTAL) TOTAL
                                    FROM          
                                        (select  dlhe.*,
                                                ((CASE WHEN FULL_TIME_FURTHER_STUDY IS NOT NULL THEN FULL_TIME_FURTHER_STUDY ELSE 0 END) + 
                                             (CASE WHEN PART_TIME_FURTHER_STUDY IS NOT NULL THEN PART_TIME_FURTHER_STUDY ELSE 0 END)) STUDY
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                              and     MOS   like 'Full-time'
                                              AND     NORTH_WEST_HEO  like 'North West HEI')
                      GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) WRK,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   WRK.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+)  )      STUDY 
              GROUP BY CAH, PROVIDER_NAME)
            GROUP BY CAH) STUDY,
            (SELECT  CAH,
                    ROUND(AVG(PROFF_PCT)) PROFF_PCT
            FROM        
              (SELECT  CAH, PROVIDER_NAME,
                      ROUND((SUM(PROFF) / SUM(TOTAL)*100)) PROFF_PCT
              FROM        
                  (SELECT DISTINCT CAH.CAH, PROVIDER_NAME, PROFF, TOTAL
                  FROM
                     (SELECT    PROFF.*,  TOTAL.TOTAL    FROM
                      (SELECT   JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) PROFF
                                        from    tbl_dlhe_detail_ft   dlhe
                                        where   academic_year in (/*'2013/14', */'2014/15', '2015/16')  --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                                          and     MOS   like 'Full-time'                                    
                                          AND     PROFESSIONAL_MARKER  =  'Professional employment'
                                          AND     NORTH_WEST_HEO  like 'North West HEI'
                                GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) PROFF,
                        (select  JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME,  SUM(GRAND_TOTAL) TOTAL
                        from    tbl_dlhe_detail_ft   dlhe  where   academic_year in (/*'2013/14', */'2014/15', '2015/16') and     MOS   like 'Full-time'  AND NORTH_WEST_HEO  like 'North West HEI' --'2013/14', --REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  -- As per LO, LB and directorate -- MF 03/01/2018
                        GROUP BY JACS_PRINCIPAL_SUBJECT_V3, PROVIDER_NAME) TOTAL
                        WHERE   PROFF.PROVIDER_NAME                 =     TOTAL.PROVIDER_NAME     (+)
                        AND     PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     TOTAL.JACS_PRINCIPAL_SUBJECT_V3   (+) ) PROFF,
                      (select cah.*, 
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                from   LKP_JACS_CAH  cah) cah
                  where   PROFF.JACS_PRINCIPAL_SUBJECT_V3     =     CAH.JACS_PRINCIPAL_SUBJECT   (+) )      PROFF 
              GROUP BY CAH, PROVIDER_NAME) PROFF
            GROUP BY CAH) PROFF
          WHERE WRK.CAH   =   STUDY.CAH   (+)  
          AND   WRK.CAH   =   PROFF.CAH   (+))  NW_bnch
WHERE     DLHE.CAH      =     ALL_BNCH.CAH      (+)
AND       DLHE.CAH      =     NW_BNCH.CAH      (+)
;



-- NON-CONT
  --core metric : Continuation (This metric is the proportion of entrants who continue their studies. Full-time students are counted between their first and second year of study)
  
--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_RET_2017_CAH2" AS
SELECT  CAH,
        sum(HC) RET_HC,
        ROUND(AVG(RET_PCT))  RET_PCT
FROM
  (select  2016 yr, 
          --FAC, PU, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN Course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN Course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN Course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END) CAH,
          sum(ret_status) HC, sum(ret_include) ret,
          round((sum(ret_include) / sum(ret_status) * 100)) RET_PCT
  from    ( select ret.* 
            from vW_Retention_Output_2017 ret) RET,
            (select   ep.programme_code,
                                      jacs1.jacs_subject jacs1,
                                      jacs2.jacs_subject jacs2,
                                      jacs3.jacs_subject jacs3
                            from      eval.programme@sppu_eval ep,
                                     (select
                                      max(pk),
                                      programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      from      eval.v_text_programme_versions@sppu_eval
                                      where archived is null
                                      group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      ) epv,
                                      quercus_sppu.jacs_subject jacs1,
                                      quercus_sppu.jacs_subject jacs2,
                                      quercus_sppu.jacs_subject jacs3
                            where   ep.pk                 =   epv.programme_pk  (+)
                            and     epv.primary_subject   =   jacs1.object_id   (+)
                            and     epv.secondary_subject =   jacs2.object_id   (+)
                            and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah1,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah2
  where   RET.ID in (select  distinct person_id_number
                  from    SPPU_HESA.MV_HESES_RECREATION_1617_RTN
                  where   DC_XPSR01 = 1
                  and     scd_dup not like 'Dupilcate')
  and  RET_STATUS = 1 AND WHICH_YEAR3 ='Last year'  
  AND   RET.CODE           =   JACS.programme_code         (+)
  and       JACS.JACS1              =   CAH1.JCODE                  (+)
  and       JACS.JACS1              =   CAH2.JCODE                  (+)
                            
group by  2016, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END)          
                              
UNION

select  2015 yr, 
          --FAC, PU, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END) CAH,
          sum(ret_status) HC, sum(ret_include) ret,
          round((sum(ret_include) / sum(ret_status) * 100)) RET_PCT
  from    ( select ret.* 
            from vW_Retention_Output_2016 ret) RET,
            (select   ep.programme_code,
                                      jacs1.jacs_subject jacs1,
                                      jacs2.jacs_subject jacs2,
                                      jacs3.jacs_subject jacs3
                            from      eval.programme@sppu_eval ep,
                                     (select
                                      max(pk),
                                      programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      from      eval.v_text_programme_versions@sppu_eval
                                      where archived is null
                                      group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      ) epv,
                                      quercus_sppu.jacs_subject jacs1,
                                      quercus_sppu.jacs_subject jacs2,
                                      quercus_sppu.jacs_subject jacs3
                            where   ep.pk                 =   epv.programme_pk  (+)
                            and     epv.primary_subject   =   jacs1.object_id   (+)
                            and     epv.secondary_subject =   jacs2.object_id   (+)
                            and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah1,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah2
  where   RET.ID in (select  distinct person_id_number
                  from    SPPU_HESA.MV_HESES_RECREATION_1516_RTN
                  where   DC_XPSR01 = 1
                  and     scd_dup not like 'Dupilcate')
  and  RET_STATUS = 1 AND WHICH_YEAR2 ='Last year'  
  AND   RET.CODE           =   JACS.programme_code         (+)
  and       JACS.JACS1              =   CAH1.JCODE                  (+)
  and       JACS.JACS1              =   CAH2.JCODE                  (+)
                            
group by  2015, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END)            

--REMOVE THIRD YEAR AS NOT IN FINAL TEASOF  
-- As per LO, LB and directorate -- MF 03/01/2018  

/*                              
UNION

select  2014 yr, 
          --FAC, PU, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END) CAH,
          sum(ret_status) HC, sum(ret_include) ret,
          round((sum(ret_include) / sum(ret_status) * 100)) RET_PCT
  from    ( select ret.*
            from vW_Retention_Output_2015 ret) RET,
            (select   ep.programme_code,
                                      jacs1.jacs_subject jacs1,
                                      jacs2.jacs_subject jacs2,
                                      jacs3.jacs_subject jacs3
                            from      eval.programme@sppu_eval ep,
                                     (select
                                      max(pk),
                                      programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      from      eval.v_text_programme_versions@sppu_eval
                                      where archived is null
                                      group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      ) epv,
                                      quercus_sppu.jacs_subject jacs1,
                                      quercus_sppu.jacs_subject jacs2,
                                      quercus_sppu.jacs_subject jacs3
                            where   ep.pk                 =   epv.programme_pk  (+)
                            and     epv.primary_subject   =   jacs1.object_id   (+)
                            and     epv.secondary_subject =   jacs2.object_id   (+)
                            and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah1,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah2
  where   RET.ID in (select  distinct person_id_number
                  from    SPPU_HESA.MV_HESES_RECREATION_1415_RTN
                  where   DC_XPSR01 = 1
                  and     scd_dup not like 'Dupilcate')
  and  RET_STATUS = 1 AND WHICH_YEAR2 ='Last year'  
  AND   RET.CODE           =   JACS.programme_code         (+)
  and       JACS.JACS1              =   CAH1.JCODE                  (+)
  and       JACS.JACS1              =   CAH2.JCODE                  (+)
                            
group by  2014, 
          (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                              WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                              WHEN course LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                              WHEN course LIKE '%SPORT%' THEN 'CAH03-02'
                              ELSE 'UNK'
                              END)*/ )
GROUP BY CAH;                              







--SUPPLEMENTARY METRICS
--"Supplementary metrics will not form part of the of the initial hypothesis, but will instead be considered alongside the subject-level submission to inform the holistic judgement."  p48

-- GRADE INFLATION
  --Supplementary Metric 1: Grade inflation


--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_INF_2017_CAH2" AS
SELECT    CAH,
          (AW16 - (CASE WHEN AW07 IS NOT NULL THEN AW07
                        WHEN AW14 IS NOT NULL THEN AW14
                        WHEN AW15 IS NOT NULL THEN AW15
                        ELSE AW16
                        END))  INF
FROM
    (SELECT    AW16.CAH,
              (AW16.PCT_1 + AW16.PCT_21) AW16,
              (AW15.PCT_1 + AW15.PCT_21) AW15,
              (AW14.PCT_1 + AW14.PCT_21) AW14,
              (AW07.PCT_1 + AW07.PCT_21) AW07
    FROM        
             (select  CI_ACADEMIC_YEAR, CAH,
                     sum(N_1) N_1,
                     sum(PCT_1) PCT_1,
                     sum(N_21) N_21,
                     sum(PCT_21) PCT_21,
                     sum(N_22_3_PASS) N_22_3_PASS,
                     sum(PCT_22_3_PASS) PCT_22_3_PASS,
                     sum(N_ORD) N_ORD,
                     sum(PCT_ORD) PCT_ORD
            FROM
              (SELECT   AW.CI_ACADEMIC_YEAR, AW.CAH,
                        --2007
                        (CASE WHEN AWARD LIKE '1' THEN N ELSE NULL END)  N_1,
                        (CASE WHEN AWARD LIKE '1' THEN PCT ELSE NULL END)  PCT_1,
                        (CASE WHEN AWARD LIKE '2.1' THEN N ELSE NULL END)  N_21,
                        (CASE WHEN AWARD LIKE '2.1' THEN PCT ELSE NULL END)  PCT_21,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN N ELSE NULL END)  N_22_3_PASS,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN PCT ELSE NULL END)  PCT_22_3_PASS,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN N ELSE NULL END)  N_ORD,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN PCT ELSE NULL END)  PCT_ORD
                        
              FROM 
                  (select award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD,
                          HC.hc, 
                          count(award.p_ID) N,
                          ROUND(count(award.p_ID) / HC.hc * 100, 1)  PCT
                  from    (SELECT AW.*, 
                          (CASE WHEN aw.G_DESC IN ('2.2','3','PASS DEGREE') THEN '2.2, 3 OR PASS'
                                ELSE aw.G_DESC
                                END) AWARD,
                           (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AW.COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.Course_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH
                                FROM VW_AWARDS_ALL_FINAL   AW,
                                     (select   ep.programme_code,
                                          jacs1.jacs_subject jacs1,
                                          jacs2.jacs_subject jacs2,
                                          jacs3.jacs_subject jacs3
                                from      eval.programme@sppu_eval ep,
                                         (select
                                          max(pk),
                                          programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          from      eval.v_text_programme_versions@sppu_eval
                                          where archived is null
                                          group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          ) epv,
                                          quercus_sppu.jacs_subject jacs1,
                                          quercus_sppu.jacs_subject jacs2,
                                          quercus_sppu.jacs_subject jacs3
                                where   ep.pk                 =   epv.programme_pk  (+)
                                and     epv.primary_subject   =   jacs1.object_id   (+)
                                and     epv.secondary_subject =   jacs2.object_id   (+)
                                and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah1,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah2
                          where   aw.course              =   JACS.programme_code         (+)
                          and       JACS.JACS1              =   CAH1.JCODE                  (+)
                          and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          and     aw.DEG = 1 
                          and     aw.CI_ACADEMIC_YEAR = 2016 
                          and     aw.mos like 'Full Time') award,
                          (select  award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH, count(award.p_ID) HC
                          from    (select aw.*,
                                         ( case     when AW.COURSE_DESC like '%[SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [SAND')))
                                                    when AW.COURSE_DESC like '[%NON-COMM%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [NON-COMM')))
                                                    when AW.COURSE_DESC like '%WITH A SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' WITH A SAND')))
                                                    when AW.COURSE_DESC like '%(SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' (SAND')))
                                                           else AW.COURSE_DESC 
                                                      end) cdesc from VW_AWARDS_ALL_FINAL aw
                                                      where   aw.mos like 'Full Time') award,
                                      (select   ep.programme_code,
                                                jacs1.jacs_subject jacs1,
                                                jacs2.jacs_subject jacs2,
                                                jacs3.jacs_subject jacs3
                                      from      eval.programme@sppu_eval ep,
                                               (select
                                                max(pk),
                                                programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                from      eval.v_text_programme_versions@sppu_eval
                                                where archived is null
                                                group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                ) epv,
                                                quercus_sppu.jacs_subject jacs1,
                                                quercus_sppu.jacs_subject jacs2,
                                                quercus_sppu.jacs_subject jacs3
                                      where   ep.pk                 =   epv.programme_pk  (+)
                                      and     epv.primary_subject   =   jacs1.object_id   (+)
                                      and     epv.secondary_subject =   jacs2.object_id   (+)
                                      and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah1,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah2
                              where   award.course              =   JACS.programme_code         (+)
                              and       JACS.JACS1              =   CAH1.JCODE                  (+)
                              and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          AND      award.DEG = 1 
                          group by award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END)) HC
     
    
                  where   award.CI_ACADEMIC_YEAR    =   HC.CI_ACADEMIC_YEAR   (+)
                  and     award.CAH                 =   hc.CAH              (+)  
                  
                  group by award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD, HC.hc) AW)
            GROUP BY CI_ACADEMIC_YEAR, CAH)  AW16,
            (select  CI_ACADEMIC_YEAR, CAH,
                     sum(N_1) N_1,
                     sum(PCT_1) PCT_1,
                     sum(N_21) N_21,
                     sum(PCT_21) PCT_21,
                     sum(N_22_3_PASS) N_22_3_PASS,
                     sum(PCT_22_3_PASS) PCT_22_3_PASS,
                     sum(N_ORD) N_ORD,
                     sum(PCT_ORD) PCT_ORD
            FROM
              (SELECT   AW.CI_ACADEMIC_YEAR, AW.CAH,
                        --2007
                        (CASE WHEN AWARD LIKE '1' THEN N ELSE NULL END)  N_1,
                        (CASE WHEN AWARD LIKE '1' THEN PCT ELSE NULL END)  PCT_1,
                        (CASE WHEN AWARD LIKE '2.1' THEN N ELSE NULL END)  N_21,
                        (CASE WHEN AWARD LIKE '2.1' THEN PCT ELSE NULL END)  PCT_21,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN N ELSE NULL END)  N_22_3_PASS,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN PCT ELSE NULL END)  PCT_22_3_PASS,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN N ELSE NULL END)  N_ORD,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN PCT ELSE NULL END)  PCT_ORD
                        
              FROM 
                  (select award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD,
                          HC.hc, 
                          count(award.p_ID) N,
                          ROUND(count(award.p_ID) / HC.hc * 100, 1)  PCT
                  from    (SELECT AW.*, 
                          (CASE WHEN aw.G_DESC IN ('2.2','3','PASS DEGREE') THEN '2.2, 3 OR PASS'
                                ELSE aw.G_DESC
                                END) AWARD,
                           (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AW.COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.Course_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH
                                FROM VW_AWARDS_ALL_FINAL   AW,
                                     (select   ep.programme_code,
                                          jacs1.jacs_subject jacs1,
                                          jacs2.jacs_subject jacs2,
                                          jacs3.jacs_subject jacs3
                                from      eval.programme@sppu_eval ep,
                                         (select
                                          max(pk),
                                          programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          from      eval.v_text_programme_versions@sppu_eval
                                          where archived is null
                                          group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          ) epv,
                                          quercus_sppu.jacs_subject jacs1,
                                          quercus_sppu.jacs_subject jacs2,
                                          quercus_sppu.jacs_subject jacs3
                                where   ep.pk                 =   epv.programme_pk  (+)
                                and     epv.primary_subject   =   jacs1.object_id   (+)
                                and     epv.secondary_subject =   jacs2.object_id   (+)
                                and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah1,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah2
                          where   aw.course              =   JACS.programme_code         (+)
                          and       JACS.JACS1              =   CAH1.JCODE                  (+)
                          and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          and     aw.DEG = 1 
                          and     aw.CI_ACADEMIC_YEAR = 2015                            
                          and     aw.mos like 'Full Time') award,
                          (select  award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH, count(award.p_ID) HC
                          from    (select aw.*,
                                         ( case     when AW.COURSE_DESC like '%[SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [SAND')))
                                                    when AW.COURSE_DESC like '[%NON-COMM%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [NON-COMM')))
                                                    when AW.COURSE_DESC like '%WITH A SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' WITH A SAND')))
                                                    when AW.COURSE_DESC like '%(SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' (SAND')))
                                                           else AW.COURSE_DESC 
                                                      end) cdesc from VW_AWARDS_ALL_FINAL aw
                                                      where aw.mos like 'Full Time') award,
                                      (select   ep.programme_code,
                                                jacs1.jacs_subject jacs1,
                                                jacs2.jacs_subject jacs2,
                                                jacs3.jacs_subject jacs3
                                      from      eval.programme@sppu_eval ep,
                                               (select
                                                max(pk),
                                                programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                from      eval.v_text_programme_versions@sppu_eval
                                                where archived is null
                                                group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                ) epv,
                                                quercus_sppu.jacs_subject jacs1,
                                                quercus_sppu.jacs_subject jacs2,
                                                quercus_sppu.jacs_subject jacs3
                                      where   ep.pk                 =   epv.programme_pk  (+)
                                      and     epv.primary_subject   =   jacs1.object_id   (+)
                                      and     epv.secondary_subject =   jacs2.object_id   (+)
                                      and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah1,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah2
                              where   award.course              =   JACS.programme_code         (+)
                              and       JACS.JACS1              =   CAH1.JCODE                  (+)
                              and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          AND      award.DEG = 1 
                          group by award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END)) HC
     
    
                  where   award.CI_ACADEMIC_YEAR    =   HC.CI_ACADEMIC_YEAR   (+)
                  and     award.CAH                 =   hc.CAH              (+)  
                  
                  group by award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD, HC.hc) AW)
            GROUP BY CI_ACADEMIC_YEAR, CAH)  AW15,
            (select  CI_ACADEMIC_YEAR, CAH,
                     sum(N_1) N_1,
                     sum(PCT_1) PCT_1,
                     sum(N_21) N_21,
                     sum(PCT_21) PCT_21,
                     sum(N_22_3_PASS) N_22_3_PASS,
                     sum(PCT_22_3_PASS) PCT_22_3_PASS,
                     sum(N_ORD) N_ORD,
                     sum(PCT_ORD) PCT_ORD
            FROM
              (SELECT   AW.CI_ACADEMIC_YEAR, AW.CAH,
                        --2007
                        (CASE WHEN AWARD LIKE '1' THEN N ELSE NULL END)  N_1,
                        (CASE WHEN AWARD LIKE '1' THEN PCT ELSE NULL END)  PCT_1,
                        (CASE WHEN AWARD LIKE '2.1' THEN N ELSE NULL END)  N_21,
                        (CASE WHEN AWARD LIKE '2.1' THEN PCT ELSE NULL END)  PCT_21,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN N ELSE NULL END)  N_22_3_PASS,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN PCT ELSE NULL END)  PCT_22_3_PASS,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN N ELSE NULL END)  N_ORD,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN PCT ELSE NULL END)  PCT_ORD
                        
              FROM 
                  (select award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD,
                          HC.hc, 
                          count(award.p_ID) N,
                          ROUND(count(award.p_ID) / HC.hc * 100, 1)  PCT
                  from    (SELECT AW.*, 
                          (CASE WHEN aw.G_DESC IN ('2.2','3','PASS DEGREE') THEN '2.2, 3 OR PASS'
                                ELSE aw.G_DESC
                                END) AWARD,
                           (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AW.COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.Course_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH
                                FROM VW_AWARDS_ALL_FINAL   AW,
                                     (select   ep.programme_code,
                                          jacs1.jacs_subject jacs1,
                                          jacs2.jacs_subject jacs2,
                                          jacs3.jacs_subject jacs3
                                from      eval.programme@sppu_eval ep,
                                         (select
                                          max(pk),
                                          programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          from      eval.v_text_programme_versions@sppu_eval
                                          where archived is null
                                          group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          ) epv,
                                          quercus_sppu.jacs_subject jacs1,
                                          quercus_sppu.jacs_subject jacs2,
                                          quercus_sppu.jacs_subject jacs3
                                where   ep.pk                 =   epv.programme_pk  (+)
                                and     epv.primary_subject   =   jacs1.object_id   (+)
                                and     epv.secondary_subject =   jacs2.object_id   (+)
                                and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah1,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah2
                          where   aw.course              =   JACS.programme_code         (+)
                          and       JACS.JACS1              =   CAH1.JCODE                  (+)
                          and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          and     aw.DEG = 1 
                          and     aw.CI_ACADEMIC_YEAR = 2014                                                       
                          and     aw.mos like 'Full Time') award,
                          (select  award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH, count(award.p_ID) HC
                          from    (select aw.*,
                                         ( case     when AW.COURSE_DESC like '%[SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [SAND')))
                                                    when AW.COURSE_DESC like '[%NON-COMM%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [NON-COMM')))
                                                    when AW.COURSE_DESC like '%WITH A SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' WITH A SAND')))
                                                    when AW.COURSE_DESC like '%(SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' (SAND')))
                                                           else AW.COURSE_DESC 
                                                      end) cdesc from VW_AWARDS_ALL_FINAL aw
                                                      where aw.mos like 'Full Time') award,
                                      (select   ep.programme_code,
                                                jacs1.jacs_subject jacs1,
                                                jacs2.jacs_subject jacs2,
                                                jacs3.jacs_subject jacs3
                                      from      eval.programme@sppu_eval ep,
                                               (select
                                                max(pk),
                                                programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                from      eval.v_text_programme_versions@sppu_eval
                                                where archived is null
                                                group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                ) epv,
                                                quercus_sppu.jacs_subject jacs1,
                                                quercus_sppu.jacs_subject jacs2,
                                                quercus_sppu.jacs_subject jacs3
                                      where   ep.pk                 =   epv.programme_pk  (+)
                                      and     epv.primary_subject   =   jacs1.object_id   (+)
                                      and     epv.secondary_subject =   jacs2.object_id   (+)
                                      and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah1,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah2
                              where   award.course              =   JACS.programme_code         (+)
                              and       JACS.JACS1              =   CAH1.JCODE                  (+)
                              and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          AND      award.DEG = 1 
                          group by award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END)) HC
     
    
                  where   award.CI_ACADEMIC_YEAR    =   HC.CI_ACADEMIC_YEAR   (+)
                  and     award.CAH                 =   hc.CAH              (+)  
                  
                  group by award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD, HC.hc) AW)
            GROUP BY CI_ACADEMIC_YEAR, CAH)  AW14,
            (select  CI_ACADEMIC_YEAR, CAH,
                     sum(N_1) N_1,
                     sum(PCT_1) PCT_1,
                     sum(N_21) N_21,
                     sum(PCT_21) PCT_21,
                     sum(N_22_3_PASS) N_22_3_PASS,
                     sum(PCT_22_3_PASS) PCT_22_3_PASS,
                     sum(N_ORD) N_ORD,
                     sum(PCT_ORD) PCT_ORD
            FROM
              (SELECT   AW.CI_ACADEMIC_YEAR, AW.CAH,
                        --2007
                        (CASE WHEN AWARD LIKE '1' THEN N ELSE NULL END)  N_1,
                        (CASE WHEN AWARD LIKE '1' THEN PCT ELSE NULL END)  PCT_1,
                        (CASE WHEN AWARD LIKE '2.1' THEN N ELSE NULL END)  N_21,
                        (CASE WHEN AWARD LIKE '2.1' THEN PCT ELSE NULL END)  PCT_21,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN N ELSE NULL END)  N_22_3_PASS,
                        (CASE WHEN AWARD LIKE '2.2, 3 OR PASS' THEN PCT ELSE NULL END)  PCT_22_3_PASS,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN N ELSE NULL END)  N_ORD,
                        (CASE WHEN AWARD LIKE 'Ordinary Degree' THEN PCT ELSE NULL END)  PCT_ORD
                        
              FROM 
                  (select award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD,
                          HC.hc, 
                          count(award.p_ID) N,
                          ROUND(count(award.p_ID) / HC.hc * 100, 1)  PCT
                  from    (SELECT AW.*, 
                          (CASE WHEN aw.G_DESC IN ('2.2','3','PASS DEGREE') THEN '2.2, 3 OR PASS'
                                ELSE aw.G_DESC
                                END) AWARD,
                           (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AW.COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AW.Course_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH
                                FROM VW_AWARDS_ALL_FINAL   AW,
                                     (select   ep.programme_code,
                                          jacs1.jacs_subject jacs1,
                                          jacs2.jacs_subject jacs2,
                                          jacs3.jacs_subject jacs3
                                from      eval.programme@sppu_eval ep,
                                         (select
                                          max(pk),
                                          programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          from      eval.v_text_programme_versions@sppu_eval
                                          where archived is null
                                          group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                          ) epv,
                                          quercus_sppu.jacs_subject jacs1,
                                          quercus_sppu.jacs_subject jacs2,
                                          quercus_sppu.jacs_subject jacs3
                                where   ep.pk                 =   epv.programme_pk  (+)
                                and     epv.primary_subject   =   jacs1.object_id   (+)
                                and     epv.secondary_subject =   jacs2.object_id   (+)
                                and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah1,
                                (select cah.*, 
                                        SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                        SUBSTR(CAH.CAH2,2,8)             CAH
                                  from   LKP_JACS_CAH  cah) cah2
                          where   aw.course              =   JACS.programme_code         (+)
                          and       JACS.JACS1              =   CAH1.JCODE                  (+)
                          and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          and     aw.DEG = 1 
                          and     aw.CI_ACADEMIC_YEAR = 2007                                                      
                          and     aw.mos like 'Full Time' ) award,
                          (select  award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH, count(award.p_ID) HC
                          from    (select aw.*,
                                         ( case     when AW.COURSE_DESC like '%[SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [SAND')))
                                                    when AW.COURSE_DESC like '[%NON-COMM%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' [NON-COMM')))
                                                    when AW.COURSE_DESC like '%WITH A SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' WITH A SAND')))
                                                    when AW.COURSE_DESC like '%(SANDWICH%' then  rtrim(substr(AW.COURSE_DESC, 1,instr(AW.COURSE_DESC,' (SAND')))
                                                           else AW.COURSE_DESC 
                                                      end) cdesc from VW_AWARDS_ALL_FINAL aw                                                                                                            
                                    where    aw.mos like 'Full Time') award,
                                      (select   ep.programme_code,
                                                jacs1.jacs_subject jacs1,
                                                jacs2.jacs_subject jacs2,
                                                jacs3.jacs_subject jacs3
                                      from      eval.programme@sppu_eval ep,
                                               (select
                                                max(pk),
                                                programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                from      eval.v_text_programme_versions@sppu_eval
                                                where archived is null
                                                group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                                ) epv,
                                                quercus_sppu.jacs_subject jacs1,
                                                quercus_sppu.jacs_subject jacs2,
                                                quercus_sppu.jacs_subject jacs3
                                      where   ep.pk                 =   epv.programme_pk  (+)
                                      and     epv.primary_subject   =   jacs1.object_id   (+)
                                      and     epv.secondary_subject =   jacs2.object_id   (+)
                                      and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah1,
                                      (select cah.*, 
                                              SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                              SUBSTR(CAH.CAH2,2,8)             CAH
                                        from   LKP_JACS_CAH  cah) cah2
                              where   award.course              =   JACS.programme_code         (+)
                              and       JACS.JACS1              =   CAH1.JCODE                  (+)
                              and       JACS.JACS1              =   CAH2.JCODE                  (+)
                          AND      award.DEG = 1 
                          group by award.CI_ACADEMIC_YEAR, (CASE WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN AWARD.CDESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN AWARD.CDESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END)) HC
     
    
                  where   award.CI_ACADEMIC_YEAR    =   HC.CI_ACADEMIC_YEAR   (+)
                  and     award.CAH                 =   hc.CAH              (+)  
                  
                  group by award.CAH, award.CI_ACADEMIC_YEAR, AWARD.AWARD, HC.hc) AW)
            GROUP BY CI_ACADEMIC_YEAR, CAH)  AW07
    where   aw16.CAH        =        aw15.CAH      (+)
    AND     aw16.CAH        =        aw14.CAH      (+)
    AND     aw16.CAH        =        aw07.CAH      (+));







-- LEO
  --Supplementary Metric 2: Sustained employment or further study
  --Supplementary Metric 3: Above median earnings threshold or further study
--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_LEO_2017_CAH2" AS
SELECT  cah.caH,
        ROUND(AVG(GRADUATES))    GRADUATES,
        ROUND(AVG(SUS_EMP))      SUS_EMP,
        ROUND(AVG(EARN_LQ))      EARN_LQ,
        ROUND(AVG(EARN_MED))     EARN_MED,
        ROUND(AVG(EARN_UQ))      EARN_UQ,
        ROUND(AVG(ABOVE_MED))    ABOVE_MED,
        ROUND(AVG(PCT))          PCT        
from    (SELECT * FROM TBL_LEO_2016 WHERE EARN_LQ NOT LIKE 'N/A') leo,
        (select cah.*, 
                SUBSTR(cah.JACS_SUBJECT_AREA,5)   PROGRAMME,
                SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                SUBSTR(CAH.CAH2,2,8)             CAH
        from   LKP_JACS_CAH  cah) cah
where   leo.programme     =     CAH.PROGRAMME   (+)
GROUP BY CAH.CAH
 ; 



 
-- teaching intensity
-- "we have concluded that teaching intensity should measure contact time in at least 2 dimensions, hours and group size."
-- "A provider declaration of the contact hours they are providing, weighted by staff-student ratios, to get a measure of teaching intensity (using a �weighted contact hours� measure as well as taking into account provision such as placements, field work and e-learning)."
-- Example includes 'Gross Teaching Quotient, Placements & Fieldwork and e-learning' (using e-val mod teaching and placement (as GTO) and placement data; no idea what 'e-learning' means) 
-- using 2016 data. Should be 2017?
  --Supplementary Metric 4: Grup Size (SSR)
  --Supplementary Metric 5: Contact hours (eVal)  
--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_TEF_TEACH_2017_CAH2" AS
select    cah,
          sum(HC_1) HC_1,   
          sum(TEACH_1)   TEACH_1,
          sum(HC_2) HC_2,
          sum(TEACH_2)   TEACH_2,
          sum(HC_3) HC_3,
          sum(TEACH_3)   TEACH_3
from          
    (select    cah,
              (case when ci_course_year like 1 then headcount else null end) HC_1,
              (case when ci_course_year like 1 then teach else null end) TEACH_1,
              (case when ci_course_year like 2 then headcount else null end) HC_2,
              (case when ci_course_year like 2 then teach else null end) TEACH_2,
              (case when ci_course_year like 3 then headcount else null end) HC_3,
              (case when ci_course_year like 3 then teach else null end) TEACH_3
    from
      (Select    cah,
                CI_COURSE_YEAR,
                headcount,
                (actual.T_avg + p_avg)  teach
      from          
          --actual student data
          (Select  CAH, CI_COURSE_YEAR,
                  count(p_ID) headcount,
                  ROUND(AVG(CW_PCT)) cw_avg,
                  ROUND(AVG(PR_PCT)) pr_avg,
                  ROUND(AVG(EX_PCT)) ex_avg,
                  ROUND(AVG(PCT_T_HRS)) T_avg,
                  ROUND(AVG(PCT_P_HRS)) p_avg,
                  ROUND(AVG(PCT_I_HRS)) I_avg
          from
              (SELECT  distinct P_ID,	CI_COURSE_YEAR,	M_LEVEL_DESC,	MOS,	C_LVL,	FAC,	PU,	COURSE_COURSE_CODE,	COURSE_DESC,	CREDITS,	
                      CW_PCT,	PR_PCT,	
                      -- need to account for rounding
                      (CASE WHEN EX_PCT + CW_PCT + PR_PCT = 100 then EX_PCT
                            when EX_PCT + CW_PCT + PR_PCT = 101 then EX_PCT - 1
                            when EX_PCT + CW_PCT + PR_PCT = 99 then EX_PCT + 1
                            else  EX_PCT
                            end) EX_PCT,   
                      T_HRS,	P_HRS,	I_HRS,	EX_T_HRS,	EX_P_HRS,	ASS_TOT,	LT_TOT,
                      PCT_T_HRS,	PCT_P_HRS,
                      -- need to account for rounding
                      (CASE WHEN PCT_T_HRS+PCT_P_HRS+PCT_I_HRS = 100 then PCT_I_HRS
                            WHEN PCT_T_HRS+PCT_P_HRS+PCT_I_HRS = 101 then PCT_I_HRS - 1              
                            WHEN PCT_T_HRS+PCT_P_HRS+PCT_I_HRS = 99 then PCT_I_HRS + 1
                            else 9999999
                            end) PCT_I_HRS,
                        JACS.JACS1,
                       (CASE  when jacs1 like 'B7' then 'CAH02-01'
                              when jacs1 like 'X2' then 'CAH22-01'
                              when jacs1 like 'F8' then 'CAH12-01'
                                  WHEN CAH1.CAH IS NOT NULL THEN CAH1.CAH
                                  WHEN CAH2.CAH IS NOT NULL THEN CAH2.CAH
                                  WHEN COURSE_DESC LIKE '%EDUCATION%' THEN 'CAH22-01'  -- LOTS OF MISSING FoE JACS CODES
                                  WHEN COURSE_DESC LIKE '%TEACHING%' THEN 'CAH22-01'   -- LOTS OF MISSING FoE JACS CODES
                                  WHEN Course_DESC LIKE '%SPORT%' THEN 'CAH03-02'
                                  ELSE 'UNK'
                                  END) CAH
              FROM
                (select  dat.*,
                        EX_T_HRS,
                        EX_P_HRS,
                          (CW_PCT + PR_PCT + EX_PCT) ASS_TOT,
                          (T_HRS + P_HRS + I_HRS) LT_TOT,
                          --have to add module and extra data and calc pctage
                          ROUND((CASE WHEN T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end) = 0 then 0 else ((T_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end))/ (T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end)) * 100) end))    PCT_T_HRS, 
                          ROUND((CASE WHEN T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end) = 0 then 0 else ((P_HRS + (case when EX_P_HRS is not null then EX_P_HRS else 0 end))/ (T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end)) * 100) end))       PCT_P_HRS, 
                          ROUND((CASE WHEN T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end) = 0 then 0 else (I_HRS/ (T_HRS+P_HRS+I_HRS + (case when EX_T_HRS is not null then EX_T_HRS else 0 end) + (case when EX_P_HRS is not null then EX_P_HRS else 0 end)) * 100) end))         PCT_I_HRS
                from
                  (select    P_ID,
                            ci_course_Year,
                            m_level_desc,
                            MOS,
                            C_LVL,
                            FAC,
                            PU,
                            COURSE_COURSE_CODE,
                            COURSE_DESC,
                            SUM(CREDITS) CREDITS,
                            ROUND(AVG(CW_PCT))   CW_PCT,	  ROUND(AVG(PR_PCT))   PR_PCT,	  ROUND(AVG(EX_PCT))   EX_PCT,	            
                            SUM(T_HRS)    T_HRS,	  SUM(P_HRS)    P_HRS,	  SUM(I_HRS)    I_HRS
                  from
                        (select  --DISTINCT SOM.M_MODULE_CODE, SOM.M_SHRT_NAME, SOM.M_MODULE_DESC, SOM.HMI_CREDIT_VALUE, SOM.HMI_HESA_CREDIT_LEVEL,
                                --SOM.HMI_MODULE_TITLE,
                                DISTINCT ( case when eval.CW_PCT like 2540 then 100
                                  else eval.CW_PCT
                                  end) CW_PCT,              -- problem with lit mod MF 03-08-17      
                                eval.PR_PCT,   
                                eval.EX_PCT ,   
                                eval.T_HRS,
                                (case when m_level_desc like 'PLACEMENT' then (credits * 10)
                                      else eval.P_HRS
                                      end) P_HRS,
                                (case when m_level_desc like 'PLACEMENT' then 0
                                      else eval.I_HRS
                                      end) I_HRS,
                                -- placements are classed as 100% teach and 0% study 
                                eval.credits,
                                SOC.*,
                                pf.fac,
                                LVL.DESCRIPTION c_lvl,
                                MOS.DESCRIPTION mos,
                                som.*
                        from    mv_student_on_module        SOM,
                                mv_student_on_COURSE_2016   SOC,
                                VW_EVAL_DATA_LT_ASS         EVAL,
                                quercus_sppu.course_level   lvl,
                                quercus_sppu.mode_of_study  mos,
                                sppu_user.lkp_pu_fac        pf
                        where   SOM.SC_SCD_OBID       (+) =     SOC.SCD_OBID         
                        AND     SOM.M_SHRT_NAME           =     EVAL.MODULE_CODE      (+)
                        and     soc.COURSE_COURSE_LEVEL   =     LVL.OBJECT_ID         (+)
                        and     soc.COURSE_MODE_OF_STUDY  =     MOS.OBJECT_ID         (+)
                        and     soc.pu                    =     pf.pu                 (+)
                        and     soc.scd_status_desc like 'Registered'
                        and     soc.mode_of_study_desc like 'Full Time'
                        and     SOM.SC_SUBJECT_REG_STATUS_DESC in('Completed', 'REGISTERED')
                        and     soc.ci_instance_code not like '%TEMP'
                        and     soc.ci_instance_code not like '%EXT'
                        and     EVAL.MODULE_CODE not like 'PUP2232' -- problem with mod MF 03-08-17 
                        and     credits is not null
                        ) STUD
                  
                  group by P_ID,        ci_course_Year, m_level_desc, MOS,          C_LVL,          FAC,          PU,          COURSE_COURSE_CODE,         COURSE_DESC
                  ) dat,
                -- EXTRA NON-MODULE DATA
                    (select course_course_code, tl_level, LVL, sum(EX_T_HRS) EX_T_HRS, sum(EX_P_HRS) EX_P_HRS
                    from
                        ( select course_course_code, tl_level, lt_type, LVL,
                              (case when extra.lt_type like 'T' then extra.hours else 0 end) EX_T_HRS,
                              (case when extra.lt_type like 'P' then extra.hours else 0 end) EX_P_HRS
                      from
                          (select  course_course_code, tl_level, lt_type, 'LEVEL ' || TL_LEVEL  LVL,
                                  sum(hours) hours
                          from
                              (select  soc.*, --extra_lt.*,   
                                        extra_lt.TL_LEVEL,
                                        extra_lt.hours,
                                        (case when (extra_lt.category like 'Sched%' and extra_lt.tl_type ! = 10) then 'T' --extra teaching
                                            when extra_lt.category like 'Place%' then 'P'
                                            when (extra_lt.category like 'Sched%' and extra_lt.tl_type = 10) then 'P'  --extra placement
                                            else 'N' --neither teaching nor placement
                                            end) lt_type
                              from
                                      (select distinct soc.course_course_code
                                        from mv_student_on_COURSE_2016 SOC)       soc,
                                      (select distinct  ptl.*, pv.pk vers_pk, p.pk prog_PK, p.programme_code 
                                      from  eval.PROGRAMME_TL@sppu_eval ptl,
                                      (select * from eval.v_text_programme_versions@sppu_eval where PK in (select max (PK) from eval.v_text_programme_versions@sppu_eval group by programme_pk)) pv,
                                      eval.programme@sppu_eval p
                                      where p.pk                    (+) =  pv.programme_pk       
                                      and     pv.pk                   (+) =  ptl.programme_version 
                                      
                                      order by programme_version, tl_level) extra_lt
                                  where     soc.course_course_code      =     extra_lt.programme_code)
                          group by course_course_code, tl_level, lt_type      ) extra) extra
                          group by course_course_code, tl_level,  LVL) extra
                where dat.COURSE_COURSE_CODE    =     extra.course_course_code        (+)
                and   dat.m_level_desc          =     extra.LVL                  (+)) dat,
                (select   ep.programme_code,
                                      jacs1.jacs_subject jacs1,
                                      jacs2.jacs_subject jacs2,
                                      jacs3.jacs_subject jacs3
                            from      eval.programme@sppu_eval ep,
                                     (select
                                      max(pk),
                                      programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      from      eval.v_text_programme_versions@sppu_eval
                                      where archived is null
                                      group by programme_pk,primary_subject,secondary_subject,tertiary_subject,ucas_code
                                      ) epv,
                                      quercus_sppu.jacs_subject jacs1,
                                      quercus_sppu.jacs_subject jacs2,
                                      quercus_sppu.jacs_subject jacs3
                            where   ep.pk                 =   epv.programme_pk  (+)
                            and     epv.primary_subject   =   jacs1.object_id   (+)
                            and     epv.secondary_subject =   jacs2.object_id   (+)
                            and     epv.tertiary_subject  =   jacs3.object_id   (+))   JACS,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_4DIGIT,2,4)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah1,
                            (select cah.*, 
                                    SUBSTR(CAH.JACS_PRINCIPAL_SUBJECT,2,2)       JCODE,
                                    SUBSTR(CAH.CAH2,2,8)             CAH
                              from   LKP_JACS_CAH  cah) cah2
where       dat.COURSE_COURSE_CODE  =   JACS.programme_code         (+)
  and       JACS.JACS1              =   CAH1.JCODE                  (+)
  and       JACS.JACS1              =   CAH2.JCODE                  (+)    ) stud
          GROUP by cah, stud.CI_COURSE_YEAR) actual
    ))
group by cah    
      ;
