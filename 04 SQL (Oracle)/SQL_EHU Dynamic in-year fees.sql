-- QUERY TO CALCULATE IN_YEAR FEE TOTAL
-- DONE DYNAMICALLY AS BASED ON CURRENT DATA AND DYNAMIC ADJUSTMENTS
-- ADJUSTMENTS = IN-YR LOSS & IN-YR STARTERS

--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_DYNAMIC_FEES_2017" as

Select  FEE.*,  ADJ.ADJUSTMENT, fee.fee_payable * ADJ.ADJUSTMENT PROJ_FEE,
        (case when fee.status in ('Registered') then FEE.Total_fee * ADJ.ADJUSTMENT else fee.fee_payable end) PROJ_FEE2,
        -- ONLY USE TOTAL FEE RATHER THAN DYNAMIC TIME-SENSITIVE MODEL THAT EXCLUDES THOSE WITH A WDFEE
        -- THIS IS BECAUSE LATE PROCESSING OF ADMIN WILL MASK TRUE WITHDAWL RATE THROUGHOUT AND WILL THEREFORE MAKE THE MODEL WROING
        hc.hesa_teacher_training,
        -- Schools direct HTT is 'G'
        (case when hc.hesa_teacher_training like 'G' then 1
              WHEN CI.course_stream in (226165094,242881013,242881031) THEN 1
         else 0
         end) SD,
         hsi.start_date,
         (case when hsi.start_date >= '01-AUG-2017' then 1 else 0 end) intake
from    (SELECT ID	,SCD	,SCD_TS	,SNAME	,EMAIL	,SHORT_STAT	,STATUS	,HIN2017	HIN ,COUNTRY	,DOMICILE	,HESA_DOM	,QUALENT2	,QUALENT3	,ENROL_DATE	,COMDATE	,ENDDATE	,LEAVE_DATE	,WD_ACTIONED	,FTE_PERCENT	,
                TARIFF	,AGE	,DOB	,SEX	,ETH	,NATIONALITY	,STUDENT_CAT	,PU	, DEPT	,FAC	,COURSE	,COURSE_OBID	,CNAME	,ICODE	,CYR	,CTYPE	,FRAN	,CMODE	,COURSEAIM	,C_LEVEL	,COURSE_LEVEL	,LOCATION	,
                FUNDER	,FUNDER2	,COURSE_TYPE	,APRIL_COHORT	,TEMP_INST	,EXCL_HEALTHTEMPS	,COURSE_FINISHED	,TRANSFER_DATE	,TTO	,CI_START	,CI_END	,CI	,SNCFLAG	,REALSNCFLAG	,ADMSNCFLAG	,ABBFLAG	,REALABBFLAG	,
                ADMABBFLAG	,NEITHERFLAG	,REALNEITHERFLAG	,LEFTEARLYFLAG	,REGDFLAG	,MYSNCABBFUNDCODE	,MYSNCABB	,ADMSNCABB	,TOPUP_EXEMPT	,EHU_TOPUP	,HOWLONG	,GONE	,ELQ_EX	,QELQ	,REDUCEDI	,FUNDCODE	,
                FUND17	,FUND16	,'NA' FUND15,EXCL_SNC14	,EXCL_HEFCE	,EXCL_UG	,EXCL_CI	,EXCL_RET_HESA	,EXCL_REDUCED	,EXCL_STATUS	,EXCL_MOS	,EXCL_DOM	,EXCL_NOT_OURS	,EXCL_LEFT	,EXCL_ELQ	,NO_QUALENT	,
                SNC_OR_EXEMPT	,ALEVSABB	,BTECABB	,OCRNEDABB	,CACHEABB	,INTBACABB	,HIGHERABB	,SAHABB	,IRISHABB	,PDABB	,ADVDIPABB	,LANDABB	,PREUABB	,ACCESSHEABB	,QE2GRAD	,QE3GRAD	,ABBRESULT	,PARTENROLLEDFLAG	,
                PELOAN	,NEWLOANFLAG	,NEWSELFFLAG	,NEWZEROFLAG	,NEWTBCFLAG	,NEWNOFEEFLAG	,NEWLOANYR1	,NEWSELFYR1	,NEWZEROYR1	,NEWTBCYR1	,NEWNOFEEYR1	,NEWLOANYR2	,NEWSELFYR2	,NEWZEROYR2	,NEWTBCYR2	,NEWNOFEEYR2	,
                OLDLOANFLAG	,OLDSELFFLAG	,OLDZEROFLAG	,OLDTBCFLAG	,OLDNOFEEFLAG	,UNKLOANFLAG	,UNKSELFFLAG	,UNKZEROFLAG	,UNKTBCFLAG	,UNKNOFEEFLAG	,UNK_SCHEME	,RESLOANFLAG	,RESSELFFLAG	,RESZEROFLAG	,RESTBCFLAG	,
                RESNOFEEFLAG	,RES_SCHEME	,FUNDLEV	,TUTFEE	,FUNDING	,NEW_STUDENT	,SLC_LOAN	,ENROLLEDLOAN	,LOAN_STATUS	,SLC_CONFIRM	,FEE_CAT	,FEE_SCHEME	,TOPUP	,FEE_LEVEL	,SSN	,FEE_NOTE	,FEE_NOTE_CLOSE_DATE	,
                NSP_NOTE	,ASSESSED_FEE	,COURSE_FEE	,MODULE_FEE	,TOTAL_FEE	,WDFEE_OLD	,WDFEE_STD	,WDFEE_SLC	,WDFEE_TOTAL	,HESA_GROSSFEE	,HESA_NETFEE	,ANY_FEE	,FEE_PAYABLE_2017	FEE_PAYABLE ,ENROLLEDFEE	,SELF_FEE	,
                OVERSEAS_SELF_FEE	,HOME_SELF_FEE	,TBC_FEE	,ZEROLOAN_FEE	,LOAN_FEE	,ENTERPRISE_FLAG	,PD_FLAG	,OVERSEAS_FLAG	,
        2017 ci_academic_year,
        (CASE WHEN FEE17.CYR < 3 THEN TO_CHAR(FEE17.CYR) ELSE '3+' END) CYR_ALT 
          FROM MV_FEE_SNC_17_TIDY FEE17
        -- NEED TO GET STARTERS WHO COMMENCE AFTER TODAY's SYSDATE
        UNION
        select  ID	,SCD	,SCD_TS	,SNAME	,EMAIL	,SHORT_STAT	,STATUS	,HIN2016	HIN ,COUNTRY	,DOMICILE	,HESA_DOM	,QUALENT2	,QUALENT3	,ENROL_DATE	,COMDATE	,ENDDATE	,LEAVE_DATE	,WD_ACTIONED	,FTE_PERCENT	,
                TARIFF	,AGE	,DOB	,SEX	,ETH	,NATIONALITY	,STUDENT_CAT	,PU	, DEPT	,FAC	,COURSE	,COURSE_OBID	,CNAME	,ICODE	,CYR	,CTYPE	,FRAN	,CMODE	,COURSEAIM	,C_LEVEL	,COURSE_LEVEL	,LOCATION	,
                FUNDER	,FUNDER FUNDER2	,COURSE_TYPE	,APRIL_COHORT	,TEMP_INST	,EXCL_HEALTHTEMPS	,COURSE_FINISHED	,TRANSFER_DATE	,TTO	,CI_START	,CI_END	,CI	,SNCFLAG	,REALSNCFLAG	,ADMSNCFLAG	,ABBFLAG	,REALABBFLAG	,
                ADMABBFLAG	,NEITHERFLAG	,REALNEITHERFLAG	,LEFTEARLYFLAG	,REGDFLAG	,MYSNCABBFUNDCODE	,MYSNCABB	,ADMSNCABB	,TOPUP_EXEMPT	,EHU_TOPUP	,HOWLONG	,GONE	,ELQ_EX	,QELQ	,REDUCEDI	,FUNDCODE	,
                'NA' FUND17, FUND16	,FUND15	,EXCL_SNC14	,EXCL_HEFCE	,EXCL_UG	,EXCL_CI	,EXCL_RET_HESA	,EXCL_REDUCED	,EXCL_STATUS	,EXCL_MOS	,EXCL_DOM	,EXCL_NOT_OURS	,EXCL_LEFT	,EXCL_ELQ	,NO_QUALENT	,
                SNC_OR_EXEMPT	,ALEVSABB	,BTECABB	,OCRNEDABB	,CACHEABB	,INTBACABB	,HIGHERABB	,SAHABB	,IRISHABB	,PDABB	,ADVDIPABB	,LANDABB	,PREUABB	,ACCESSHEABB	,QE2GRAD	,QE3GRAD	,ABBRESULT	,PARTENROLLEDFLAG	,
                PELOAN	,NEWLOANFLAG	,NEWSELFFLAG	,NEWZEROFLAG	,NEWTBCFLAG	,NEWNOFEEFLAG	,NEWLOANYR1	,NEWSELFYR1	,NEWZEROYR1	,NEWTBCYR1	,NEWNOFEEYR1	,NEWLOANYR2	,NEWSELFYR2	,NEWZEROYR2	,NEWTBCYR2	,NEWNOFEEYR2	,
                OLDLOANFLAG	,OLDSELFFLAG	,OLDZEROFLAG	,OLDTBCFLAG	,OLDNOFEEFLAG	,UNKLOANFLAG	,UNKSELFFLAG	,UNKZEROFLAG	,UNKTBCFLAG	,UNKNOFEEFLAG	,UNK_SCHEME	,RESLOANFLAG	,RESSELFFLAG	,RESZEROFLAG	,RESTBCFLAG	,
                RESNOFEEFLAG	,RES_SCHEME	,FUNDLEV	,TUTFEE	,FUNDING	,NEW_STUDENT	,SLC_LOAN	,ENROLLEDLOAN	,LOAN_STATUS	,SLC_CONFIRM	,FEE_CAT	,FEE_SCHEME	,TOPUP	,FEE_LEVEL	,SSN	,FEE_NOTE	,FEE_NOTE_CLOSE_DATE	,
                NSP_NOTE	,ASSESSED_FEE	,COURSE_FEE	,MODULE_FEE	,TOTAL_FEE	,WDFEE_OLD	,WDFEE_STD	,WDFEE_SLC	,WDFEE_TOTAL	,HESA_GROSSFEE	,HESA_NETFEE	,ANY_FEE	,FEE_PAYABLE_2016	FEE_PAYABLE ,ENROLLEDFEE	,SELF_FEE	,
                OVERSEAS_SELF_FEE	,HOME_SELF_FEE	,TBC_FEE	,ZEROLOAN_FEE	,LOAN_FEE	,ENTERPRISE_FLAG	,PD_FLAG	,OVERSEAS_FLAG	, 
        2016 ci_academic_year,
        (CASE WHEN FEE16.CYR < 3 THEN TO_CHAR(FEE16.CYR) ELSE '3+' END) CYR_ALT 
        from MV_FEE_SNC_16_TIDY FEE16
        where FEE16.comdate >= add_months(SYSDATE , -12)) FEE,
        VW_FEES_2016_ADJUSTMENTS_DYNO ADJ,
        -- rewrite adj query so it is also dynamic        
        quercus_sppu.hesa_course  hc,
        mv_student_on_course_anyyr soc,
        quercus_sppu.hesa_student_instance hsi,
        quercus_sppu.course_instance_table  ci
WHERE   FEE.FAC        =     ADJ.FAC       (+)
AND     FEE.C_LEVEL    =     ADJ.C_LEVEL   (+)
AND     FEE.CMODE      =     ADJ.CMODE     (+)
AND     FEE.CYR_ALT    =     ADJ.CYR       (+)
and     fee.course_OBID       =   HC.COURSE             (+)
and     fee.ci_academic_year  =   HC.ACADEMIC_SESSION   (+)
and     fee.scd               =   soc.scd_obid          (+)
and     soc.ci_obid           =   ci.object_id          (+)
and     soc.scd_obid          =   hsi.student_course_detail     (+)
;






-- THE VIEW TO DYNAMICALLY CALCULATE ADJUSTMENT RATE FOR PARTICULAR GROUPS
--CREATE OR REPLACE FORCE VIEW "SPPU_USER"."VW_FEES_2016_ADJUSTMENTS_DYNO" as
select  FAC,
        C_LEVEL,
        CMODE,
        (CASE WHEN CYR < 3 THEN TO_CHAR(CYR)
              ELSE '3+'
              END) CYR,
        count(ID) cnt,      
        SUM(TOTAL_FEE) TOTAL_FEE, 
        sum(ENROLLEDFEE) ENROLLED_FEE,
        (CASE WHEN SUM(TOTAL_FEE) IS NULL THEN 0
        WHEN SUM(TOTAL_FEE) = 0 THEN 0
        ELSE SUM(ENROLLEDFEE) / SUM(TOTAL_FEE)
        END) ADJUSTMENT
from    (select * from MV_FEE_SNC_16_TIDY 
        where (status not like '%Withdrawn%' AND ENDDATE is NULL or ENDDATE >= add_months(sysdate, -12)   OR (status like '%Withdrawn%' AND wd_actioned >= add_months(sysdate, -12)) )  )
        -- only include loss after today's sysdate
where   short_stat like 'Reg'
and     fran in ('O','V','F')
and     status in ('Registered','Interrupted','Registered And Withdrawn','Transferred','Interrupted And Withdrawn (within same session)')
group by FAC, C_LEVEL, CMODE, (CASE WHEN CYR < 3 THEN TO_CHAR(CYR) ELSE '3+' END)

 
; 
 
 
 
