
C:.
|   Directory_Tree.txt
|   
+---01 PowerBI
|       DLHE Summary.pdf
|       Project Logframe.pdf
|       
+---02 R
|       Data transformation example.R
|       Logistic Regression example.R
|       
+---03 Machine Learning (Python)
|   +---scikit-learn
|   |       scikit-learn data.xlsx
|   |       Supervised Learning example.ipynb
|   |       Unsupervised Learning example.ipynb
|   |       
|   \---TensorFlow
|           Image classification example (mosquito).ipynb
|           
+---04 SQL (Oracle)
|       EHU TEF2 simulation.sql
|       SQL_EHU Dynamic in-year fees.sql
|       
+---05 Excel
|       Form example (sample database).xlsx
|       Macro example (Stakeholder Analysis).xlsm
|       
+---06 User Guides
|       GIT Guide.pdf
|       Organising Data Guide.pdf
|       SharePoint - Library Sync.pdf
|       
\---07 Reports
        Learning Gain example.pdf
        Uol Careers Analysis.pdf
        
